# oscloud

Inject Openstack clouds.yaml as environment variables in processes. No more
openrc/keystonerc files lying around.

## Why?

Are you still using openrc/keystonerc files that you source as you want to use
your differents Openstack accounts. `clouds.yaml` makes it much easier to
manage your clouds credentials. But some legacy applications still use the
environment variables, thus the need to maintain both. But with oscloud, you
can either load your clouds in your current environment variables or spawn a
process with your cloud credentials.

## Requirements

  - Openstack credentials
  - Python 3
  - Virtualenv and Pip

## Usage

1. Create a clouds.yaml file with your clouds definition. Full documentation is
   [here](https://docs.openstack.org/python-openstackclient/latest/configuration/).
   You can validate your cloud entries with:

    ```!bash
    openstack --os-cloud=<cloud> configuration show
    ```

2. Install oscloud:

    ```!bash
    git clone https://gitlab.org/spiette/oscloud
    cd oscloud
    virtualenv-3 venv
    source venv/bin/activate
    python setup.py install
    mkdir -p ~/bin
    ln -sfr venv/bin/oscloud ~/bin/
    ```

3. Use oscloud. Make sure ~/bin is in your `$PATH`

    ```!bash
    oscloud <cloud> nova list
    oscloud <cloud>
    $ nova list
    eval "$(oscloud --export <cloud>)"
    $ glance image-list
    ```
