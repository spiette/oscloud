#!/usr/bin/env python3

# Spawn a process using clouds.yaml to populate OS_* environment variables

import argparse
import os
import sys
import openstack


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('cloud', help='clouds.yaml entry')
    parser.add_argument(
            '--export', '-e',
            help='show export shell commands and exit (show password)',
            action='store_true',
            default=False)

    parser.add_argument('--verbose', '-u', help='clouds.yaml entry',
                        action='store_true',
                        default=False, )
    parser.add_argument('command', help='command to execute',
                        nargs=argparse.REMAINDER)
    return parser.parse_args()


def get_cloud_environ(cloud):
    conn = openstack.connect(cloud=cloud)
    config = conn.config.config
    auth = config['auth']

    # OS_CLOUD_NAME can be used in the child process
    environ = {
        'OS_CLOUD_CONFIG_NAME': cloud,
        'OS_CACERT': config.get('cacert'),
        'OS_VOLUME_API_VERSION': config.get('volume_api_version'),
        'OS_VERIFY': str(config.get('verify')),
        'OS_FOOBAR': config.get('foobar'),
        'OS_REGION_NAME': config.get('region_name'),
        'OS_IDENTITY_API_VERSION': config.get('identity_api_version'),
        'OS_IDENTITY_ENDPOINT_OVERRIDE': config.get('identity_endpoint_override'), # noqa
        'OS_AUTH_URL': auth.get('auth_url'),
        'OS_USER_DOMAIN_NAME': auth.get('user_domain_name'),
        'OS_USERNAME': auth.get('username'),
        'OS_PASSWORD': auth.get('password'),
        'OS_PROJECT_DOMAIN_NAME': auth.get('project_domain_name'),
        'OS_PROJECT_NAME': auth.get('project_name'),
        'OS_TENANT_NAME': auth.get('project_name'),
    }

    return {k: v for k, v in environ.items() if v is not None}


def print_environ_debug(environ):
    for k in environ.keys():
        if k == 'OS_PASSWORD':
            print("{}: ********".format(k))
        else:
            print("{}: {}".format(k, str(environ[k])))


def main():
    args = get_args()

    environ = get_cloud_environ(args.cloud)

    if args.verbose:
        print_environ_debug(environ)
    if args.export:
        for k in environ.keys():
            print("export {}={}".format(k, str(environ[k])))
        sys.exit(0)

    os.environ.update(environ)

    command = "bash" if len(args.command) == 0 else " ".join(args.command)
    os.system(command)


if __name__ == '__main__':
    main()
