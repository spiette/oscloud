from setuptools import setup, find_packages


# See https://pypi.python.org/pypi?%3Aaction=list_classifiers
classifiers = [
    # How mature is this project? Common values are
    #   3 - Alpha
    #   4 - Beta
    #   5 - Production/Stable
    'Development Status :: 3 - Alpha',
    # Indicate who your project is intended for
    'Intended Audience :: System Administrators',
    'Topic :: System :: Systems Administration',
    # Specify the Python versions you support here. In particular, ensure
    # that you indicate whether you support Python 2, Python 3 or both.
    'Programming Language :: Python :: 3',
]

requirements = [
    'python-openstacksdk',
]

setup(
    # Lib name
    name='oscloud',
    # Current version
    version='0.0.2',
    # Lib description
    description='Spawn a process using clouds.yaml to populate OS_* environment variables',
    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=requirements,
    url="https://gitlab.com/spiette/oscloud",
    author='World GNS IaaS',
    author_email='world-gns-iaas@ubisoft.com',
    classifiers=classifiers,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'oscloud=oscloud:main',
        ]
    },
)
